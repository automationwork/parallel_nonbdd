package com.test.selenium.util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.model.Log;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.browserstack.local.Local;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.jna.platform.win32.Sspi;
import com.test.selenium.extentreport.ExtentFactory;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.Reporter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class TestBase {
    public static WebDriver driver;

    //    public static ExtentReports extent;
//    public static ExtentTest test;
    private Local l;


//    @BeforeSuite
//    public void startReport() {
//        ExtentFactory ex = new ExtentFactory();
//        extent = ex.getInstance();
//    }

    @BeforeMethod(alwaysRun = true)
    @org.testng.annotations.Parameters(value = {"config", "browsername", "runenv"})
    public void setUp(String config_file, String browsername, String runenv) throws Exception {
        if (!runenv.equalsIgnoreCase("local")) {

            JSONParser parser = new JSONParser();
            JSONObject config = (JSONObject) parser.parse(new FileReader("src/test/resources/conf/" + config_file));
            JSONObject envs = (JSONObject) config.get("environments");

            DesiredCapabilities capabilities = new DesiredCapabilities();

            Map<String, String> envCapabilities = (Map<String, String>) envs.get(browsername);
            Iterator it = envCapabilities.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
            }

            Map<String, String> commonCapabilities = (Map<String, String>) config.get("capabilities");
            it = commonCapabilities.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                if (capabilities.getCapability(pair.getKey().toString()) == null) {
                    capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
                }
            }

            String username = System.getenv("BROWSERSTACK_USERNAME");
            if (username == null) {
                username = (String) config.get("user");
            }

            String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
            if (accessKey == null) {
                accessKey = (String) config.get("key");
            }

            if (capabilities.getCapability("browserstack.local") != null && capabilities.getCapability("browserstack.local") == "true") {
                l = new Local();
                Map<String, String> options = new HashMap<String, String>();
                options.put("key", accessKey);
                l.start(options);
            }
            driver = new RemoteWebDriver(new URL("http://" + username + ":" + accessKey + "@" + config.get("server") + "/wd/hub"), capabilities);
        } else {
            if (browsername.equalsIgnoreCase("firefox")) {
                System.out.println("Opening FIREFOX browser");
                System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
                driver = new FirefoxDriver();

            } else if (browsername.equalsIgnoreCase("chrome")) {
                System.out.println("Opening CHROME browser");
                System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
                driver = new ChromeDriver();
                Reporter.log("Opening Chrome Browser");

            } else if (browsername.equalsIgnoreCase("ie")) {
                System.out.println("Opening IE browser");
                System.setProperty("webdriver.ie.driver", "src\\test\\resources\\drivers\\IEDriverServer.exe");
                driver = new InternetExplorerDriver();

            } else if (browsername.equalsIgnoreCase("chrome")) {
                System.out.println("Opening OPERA browser");
                System.setProperty("webdriver.opera.driver", "src\\test\\resources\\drivers\\operadriver.exe");
                OperaOptions options = new OperaOptions();
                options.setBinary("C:\\Program Files\\Opera\\51.0.2830.55\\opera.exe");
                driver = new OperaDriver();
            }
        }
        driver.get("http://automationpractice.com/index.php");
//        driver.manage().window().maximize();
    }

    @AfterMethod(alwaysRun = true)
    public void teardown() throws Exception {
//    public void tearDown(ITestResult result) throws Exception {
//        if (result.getStatus() == ITestResult.FAILURE) {
//            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + "TEST CASE FAIL", ExtentColor.RED));
//            test.fail(result.getThrowable());
//
//            String screenshotPath = getScreenshot(driver, result.getName());
//            test.log(Status.FAIL, String.valueOf(test.addScreenCaptureFromPath(screenshotPath))); //to add screenshot in extent report
////            test.log(Status.FAIL, (Markup) test.addScreencastFromPath(screenshotPath)); //to add screencast/video in extent report
//
//        } else if (result.getStatus() == ITestResult.SUCCESS) {
//            test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + "TEST CASE PASS", ExtentColor.GREEN));
//        } else if (result.getStatus() == ITestResult.SKIP) {
//            test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + "TEST CASE FAIL", ExtentColor.YELLOW));
//            test.fail(result.getThrowable());
//        }
        driver.quit();
        if (l != null) l.stop();
    }

//    @AfterSuite
//    public void endReport() {
//        extent.flush();
//    }

    public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
        String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
        TakesScreenshot ts = (TakesScreenshot) driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        // after execution, you could see a folder "FailedTestsScreenshots"
        // under src folder
        String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + screenshotName + dateName
                + ".png";
        File finalDestination = new File(destination);
        FileUtils.copyFile(source, finalDestination);
        return destination;
    }

    public void takeSnapshot(String methodname) throws IOException {
        String destination = System.getProperty("user.dir") + "\\FailedTestsScreenshots\\" +this.getClass().getName()+ ":"+ methodname + ".png";
        File srcfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcfile, new File(destination));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
