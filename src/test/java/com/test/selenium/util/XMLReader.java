
/**	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
 * XML READER
 *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**/

package com.test.selenium.util;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class XMLReader {
	
	String path = "";
	
	/** Parameterized Constructor to initialize XML file path
	 * @param xmlPath - XML file path value	*/
	
	public XMLReader(String xmlPath) {
		path = xmlPath;
	}
	
	/** Function 		- Read Tag Value according to the given Tag Name
	 * @param tagName 	- tag name in string format
	 * @return 			- value of tag name in string format	*/
	
	public String readTagVal(String tagName) {	
    	
		String tagVal	=	null;
        File file 		= 	new File(path);
        
        try {
        	
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc 			= builder.parse(file);
            tagVal 					= doc.getElementsByTagName(tagName).item(0).getTextContent();
            
        } catch(Exception e) {
        	
        	System.out.println("Could not get data from config.XML file for the tag "+tagName);
        
        }
    
        return tagVal;
	}
	  
}
