package com.test.selenium.util;

import org.apache.commons.lang3.StringUtils;
import org.testng.log4testng.Logger;

import java.io.*;
import java.util.Properties;


/**
 * EnvironmentPropertiesReader is to set the environment variable declaration
 * mapping for config properties in the UI test
 */
public class EnvironmentPropertiesReader {

	private static final Logger log = Logger.getLogger(EnvironmentPropertiesReader.class);
	private static EnvironmentPropertiesReader envProperties;

	private Properties properties;
	
	/***
	 * default constructor
	 */
	private EnvironmentPropertiesReader() {
		properties = loadProperties();
	}
	
	/***
	 * parameterized constructor
	 * @param propertyFile
	 */
	private EnvironmentPropertiesReader(String propertyFile) {
		properties = loadProperties(propertyFile);
	}
	
	/***
	 * function to load Properties file
	 * @return
	 */
	private Properties loadProperties() {
		
		File file 					= new File("./src/main/resources/configuration.properties");
		FileInputStream fileInput 	= null;
		Properties props 			= new Properties();

		try {
			
			fileInput 				= new FileInputStream(file);
			
			props.load(fileInput);
			fileInput.close();
			
		} catch (FileNotFoundException e) {
			log.error("config.properties is missing or corrupt : " + e.getMessage());
		} catch (IOException e) {
			log.error("read failed due to: " + e.getMessage());
		}

		return props;
	}
	
	/***
	 * overloaded function to load Properties file
	 * @param propertyFile
	 * @return
	 */
	private Properties loadProperties(String propertyFile) {
		
		File file 					= new File("./src/main/resources/"+propertyFile+".properties");
		FileInputStream fileInput 	= null;
		Properties props 			= new Properties();

		try {
			
			fileInput 				= new FileInputStream(file);
			
			props.load(fileInput);
			fileInput.close();
		} catch (FileNotFoundException e) {
			log.error(""+propertyFile+".properties is missing or corrupt : " + e.getMessage());
		} catch (IOException e) {
			log.error("read failed due to: " + e.getMessage());
		}

		return props;
	}
	
	/***
	 * function to get Instance of Properties Reader
	 * @return
	 */
	public static EnvironmentPropertiesReader getInstance() {
		
		if (envProperties == null) {
			envProperties 			= new EnvironmentPropertiesReader();
		}
		
		return envProperties;
	}
	
	/***
	 * overloaded function to get Instance of Properties Reader
	 * @return
	 */
	public static EnvironmentPropertiesReader getInstance(String propertyFile) {
		
		EnvironmentPropertiesReader envProperties = null;
		
		if (envProperties == null) {
			envProperties = new EnvironmentPropertiesReader(propertyFile);
		}
		
		return envProperties;
	}
	
	/***
	 * function to get Property on the basis of key
	 * @param key
	 * @return
	 */
	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	/***
	 * function to check property is blank or not
	 * @param key
	 * @return
	 */
	public boolean hasProperty(String key) {		
		return StringUtils.isNotBlank(properties.getProperty(key));
	}
	
	/***
	 * function to set property on the basis of key and value
	 * @param key
	 * @param value
	 * @throws IOException
	 */
	public void setProperty(String key, String value) throws IOException {
		
		String fileName 			= "./src/main/resources/configuration.properties";
		File file 					= new File(fileName);
		FileOutputStream fileOut 	= new FileOutputStream(file);
		Properties props 			= new Properties();
		props 						= properties;
		
		props.setProperty(key, value);
		
		props.store(fileOut, null);
		
		fileOut.close();
	}

}
