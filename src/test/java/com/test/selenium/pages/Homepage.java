package com.test.selenium.pages;

import com.test.selenium.util.readWrite;
import cucumber.api.java.pt.E;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Homepage {
    WebDriver driver;
    String dummydatapath = "testdata\\dummydata.txt";

    private By email = By.id("email");
    private By password = By.id("passwd");
    private By submit = By.id("SubmitLogin");
    private By error = By.cssSelector(".alert.alert-danger>ol");

    private By myacc = By.cssSelector(".navigation_page");
    private By otherfields = By.cssSelector((".myaccount-link-list>li"));

    public Homepage(WebDriver driver) {
        this.driver = driver;
    }

    public String reademail() {
        readWrite read = new readWrite();
        String dummyemail = read.reading(dummydatapath)[0];
        System.out.println("all emails in the list : \n ");
        for (String textdata : read.reading(dummydatapath)) {
            System.out.println(textdata);
        }
        return dummyemail;
    }

    public String myaccount() {
        return driver.findElement(myacc).getText();
    }

    public List Ochecks(){
        List data = new ArrayList();
        data.add("Order history and details");
        data.add("My credit slips");
        data.add("My addresses");
        data.add("My personal information");
        data.add("My wishlists");
        return  data;
    }

    public void buytops(String Pass) {

        NewUser login = new NewUser(driver);
        login.login();
        driver.findElement(email).sendKeys(reademail());
        driver.findElement(password).sendKeys(Pass);
        driver.findElement(submit).click();

        try {
            WebElement elem = driver.findElement(error);
            if (elem.getText().contains("failed")) {
                System.out.println("Username or Password has been incorrectly entered");
            }

        } catch (Exception e) {
            try {
                Assert.assertEquals(myaccount(), "My account");
                Integer x = 0;
                List<WebElement> elems = driver.findElements(otherfields);
                Iterator<WebElement> iter = elems.iterator();
                while (iter.hasNext()) {
                    WebElement item = iter.next();
                    String result = item.getText();
                    Assert.assertEquals(result, Ochecks());

                }
            } catch (Exception e1) {

            }
        }


    }
}

