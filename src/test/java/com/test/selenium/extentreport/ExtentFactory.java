package com.test.selenium.extentreport;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentFactory {

    public static ExtentReports extent;
    public static ExtentHtmlReporter htmlReporter;

    public ExtentReports getInstance() {

        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/ExtentReport.html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        extent.setSystemInfo("OS", "Windows");
        extent.setSystemInfo("Host Name", "Harwinder");
        extent.setSystemInfo("Environment", "Test-QA");

        htmlReporter.config().setDocumentTitle("Demo for paralle Testing Framework");
        htmlReporter.config().setReportName("Demeo Report by - Harwinder");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.STANDARD);

        return extent;
    }
}